import React, { useState, useEffect } from 'react'


const INITIAL_STATE = {
    lat: null,
    long: null,
    speed: null
}


const App = () => {

    const [count, setCount] = useState(0)
    const [isOn, setIsOn] = useState(false)
    const [mousePosition, setMousePosition] = useState({ x: null, y: null })
    const [status, setStatus] = useState(navigator.onLine)
    const [{lat,long,speed}, setLocation] = useState(INITIAL_STATE)

    let mounted = true

    useEffect(() => {
        document.title = `CLICKED ${count} TIMES!`
        window.addEventListener('mousemove', handleMouseMove)
        window.addEventListener('online', handleOnline)
        window.addEventListener('offline', handleOffline)
        navigator.geolocation.getCurrentPosition(handleGeolocation)
        const watchId = navigator.geolocation.watchPosition(handleGeolocation)


        return () => {
            window.removeEventListener('mousemove', handleMouseMove)
            window.removeEventListener('online', handleOnline)
            window.removeEventListener('offline', handleOffline)
            navigator.geolocation.clearWatch(watchId)
            mounted = false
        }
    }, [count])


    const handleGeolocation = (e) => {

        if (mounted) {

            setLocation({
                lat: e.coords.latitude,
                long: e.coords.longitude,
                speed: e.coords.speed
            })
        }
    }

    const handleOnline = (e) => {
        setStatus(true)
    }

    const handleOffline = (e) => {
        setStatus(false)
    }

    const handleMouseMove = (e) => {
        setMousePosition({
            x: e.pageX,
            y: e.pageY
        })
    }

    const incrementCount = () => {
        // setCount(count+1)
        setCount(prevCount => prevCount + 1)
    }

    const toggleLight = () => {
        setIsOn(prevIsOn => !prevIsOn)
    }



    return (
        <>
            <h1>Functional Component</h1>
            <button onClick={incrementCount}>
                Clicked {count} times
            </button>

            <h2>Toggle Light</h2>
            <img
                src={
                    isOn
                        ? 'https://icon.now.sh/highlight/fd0'
                        : 'https://icon.now.sh/highlight/aaa'
                }
                style={{
                    height: '50px',
                    width: '50px',
                    // background: isOn ? 'orange' : 'grey'
                }}
                alt="light-bulb"
                onClick={toggleLight} />

            <h2>Mouse Position</h2>
            <p>X: {mousePosition.x}</p>
            <p>Y: {mousePosition.y}</p>

            <h2>Network Status</h2>
            <p>
                You are <strong> {status ? 'ONLINE' : 'OFFLINE'} </strong>
            </p>

            <h2>Geolocation</h2>
            <p>
                Latitude: {lat}
                Longitude: {long}
                Speed: {speed ? speed : "0" }
            </p>
        </>
    )

}

export default App