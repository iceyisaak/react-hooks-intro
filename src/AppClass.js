import React, { Component } from 'react';


class App extends Component {
  state = {
    count: 0,
    isOn: false,
    x: null,
    y: null
  }

  componentDidMount() {
    document.title = `CLICKED ${this.state.count} TIMES`
    window.addEventListener('mousemove', this.handleMouseMove)
  }

  componentDidUpdate() {
    document.title = `CLICKED ${this.state.count} TIMES`
  }

  componentWillUnmount() {
    window.removeEventListener('mousemove', this.handleMouseMove)
  }


handleMouseMove = (e) => {
  this.setState({
    x: e.pageX,
    y: e.pageY
  })
}


  incrementCount = () => {
    this.setState(prevState => ({
      // count: this.state.count + 1
      count: prevState.count + 1
    }))
  }

  toggleLight = () => {
    this.setState(prevState => ({
      isOn: !prevState.isOn
    }))
  }


  render() {
    return (
      <>
        <h1>Class Component: </h1>

        <button onClick={this.incrementCount}>
          Clicked {this.state.count} Times
        </button>

        <h2>Toggle Light</h2>
        <div
          style={{
            height: '50px',
            width: '50px',
            background: this.state.isOn ? 'orange' : 'grey'
          }}
          onClick={this.toggleLight}>
        </div>

        <h2>Mouse Position</h2>
        <p>X: {this.state.x}</p>
        <p>Y: {this.state.y}</p>
      </>
    )
  }

}

export default App;
